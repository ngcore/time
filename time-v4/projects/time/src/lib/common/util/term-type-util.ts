import { TermType } from '../core/term-type';

export namespace TermTypeUtil {

  // temporary
  export function getDefaultTermType(): TermType {
    return TermType.daily;
  }

}
