import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateRange, DateIdUtil } from '@ngcore/core';

export * from '../core/term-type';
export * from '../util/term-type-util';


/**
 * Utility functions for date range handling.
 */
export namespace DateRangeUtil {
  // Note: All time periods (day, week, month, etc.) are [begin, end)
  //    (e.g., begin is included, but end is excluded)...

  // "dateId" format: yyyymmdd.
  // Month: 1 ~ 12.

  export function getDates(startDate: (string | null)): (string[] | null);
  export function getDates(startDate: (string | null), endDate: (string | null)): (string[] | null);
  export function getDates(startDate: (string | null), dayCount: number): (string[] | null);
  export function getDates(dayCount: number): (string[] | null);
  export function getDates(dayCount: number, endDate: (string | null)): (string[] | null);
  export function getDates(dateRange: DateRange): (string[] | null);
  export function getDates(
    arg1: (DateRange | string | number | null),
    arg2?: (string | number | null)): (string[] | null) {
    if (arg1 == null) {
      return null;
    } else {
      // [startDate, endDate)
      // TBD: It's probably better to use {startDate, dayCount} rather than {startDate, endDate} ???
      let startDate: string;
      let endDate: string;
      if ((typeof arg1) == 'string' && (typeof arg2) == 'number') {
        startDate = arg1 as string;
        let dayCount = arg2 as number;  // tbd: verify dayCount >= 0
        endDate = DateIdUtil.getNthDayId(startDate, dayCount);
      } else if ((typeof arg1) == 'string') {
      // } else if ((typeof arg1) == 'string' && (typeof arg2) == 'string') {
        startDate = arg1 as string;
        endDate = arg2 as string;
        if (!endDate) {   // tbd: check startDate <= endDate.
          endDate = DateIdUtil.getTodayId();
        }
      } else if ((typeof arg1) == 'number') {
      // } else if ((typeof arg1) == 'number' && (typeof arg2) == 'string') {
        let dayCount = arg1 as number;  // tbd: verify dayCount >= 0
        endDate = arg2 as string;
        if (!endDate) {
          endDate = DateIdUtil.getTodayId();
        }
        startDate = DateIdUtil.getNthDayId(endDate, -dayCount);
      } else if (arg1 instanceof DateRange) {
        let dateRange = arg1 as DateRange;
        startDate = dateRange.startDate;
        endDate = dateRange.endDate;
      } else {
        // This should not happen...
      }
      // TBD: validate startDate/endDate? (e.g., startDate <= endDate?)
      // temporary
      const maxDays = 10000;  // ~30 years
      let dayCnter = 0;
      let dates: string[] = [];
      if (startDate && endDate) {
        let d = startDate;
        while (+d < +endDate && dayCnter++ < maxDays) {
          // if(isDL()) dl.log(`>>> date = ${d}`)
          dates.push(d);
          d = DateIdUtil.getNextDayId(d);
        }
      }
      // dates = dates.reverse();
      return dates;
    }
  }

  // export function getDatesForRange(dateRange: DateRange): (string[] | null) {
  //   return getDates(dateRange.startDate, dateRange.endDate);
  // }
  // export function getDatesFromStartDate(startDate: (string | null), dayCount: number): (string[] | null) {
  //   return getDatesForRange(new DateRange(startDate, dayCount));
  // }
  // export function getDatesFromEndDate(dayCount: number, endDate: (string | null)): (string[] | null) {
  //   let startDate = DateIdUtil.getNthDayId(endDate, -dayCount);
  //   return getDatesForRange(new DateRange(startDate, endDate));
  // }


  // Placeholders...

  export function isDayHourValid(dayHour: string): boolean {
    return false;
  }

  // getTermTime()
  // ....

}
