import { async, inject, TestBed } from '@angular/core/testing';
import {} from 'jasmine';

import { DateRangeUtil } from './date-range-util';


// https://angular.io/docs/ts/latest/guide/testing.html

describe('getDates', () => {
  let startDate = '20171015';
  let endDate = '20190125';

  beforeEach(async(() => {
  }));

  it ('getDates called by startDate and endDate', () => {
    let range = DateRangeUtil.getDates(startDate, endDate);
    expect(range).not.toBeNull();
    console.log(range);
  });



});
