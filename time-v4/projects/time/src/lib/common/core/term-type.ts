/**
 * Defines "term" type. Daily, monthly, etc...
 */
export enum TermType {
  unknown = 0,
  hourly,
  daily,
  weekly,
  monthly,
  // yearly,
  // cumulative, // ???
  // current,    // ???
  // ???
}
