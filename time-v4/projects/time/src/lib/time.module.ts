import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { NgCoreCoreModule } from '@ngcore/core';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    // BrowserModule,

    NgCoreCoreModule.forRoot()
  ],
  declarations: [
    // CommonTimeButtonComponent
  ],
  exports: [
    // CommonTimeButtonComponent
  ],
  entryComponents: [
    // CommonTimeButtonComponent
  ]
})
export class NgCoreTimeModule {
  static forRoot(): ModuleWithProviders<NgCoreTimeModule> {
    return {
      ngModule: NgCoreTimeModule,
      providers: [
        // CommonTimeService
      ]
    };
  }
}
