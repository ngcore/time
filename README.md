# @ngcore/time
> NG Core angular/typescript time library


Time/date related classes for Angular.
(Current version requires Angular v7.2+)


## API Docs

* http://ngcore.gitlab.io/time/



